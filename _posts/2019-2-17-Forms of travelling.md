---
layout: post
title: Forms of travelling
---

A person might employ one of these three forms of travelling depending on factors
such as comfort, safety, time, and expense. 

Travelling by plane is the first form of travelling which is the quickest,
safest, and most expensive of all; however, it is not quite comfortable. It is often employed when the distance
between the origin and the destination is considerable; for the other methods
are unable to satisfy the flight passengers' demands for so high a speed. 

The second form is travelling by train which in long-distance trips offers low cost, great comfort and is safe; but, it is not as quick as an aeroplane.
People usually travel by train when comfort matters to them and when they wish to feel 'at home'.

Travelling by car is the third form of travelling which is neither as
comfortable nor as safe in long-distance trips; nonetheless, it is the best form in short-distance trips, as it offers good speed, low cost, and acceptable comfort.

Overall, which form of travelling to employ is a question of distance,
time, expense and comfort which, in most cases, is thoroughly up to the traveller's choice.




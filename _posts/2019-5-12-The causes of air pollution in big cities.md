---
layout: post
title: Why is air polluted in big cities? 
---


Big cities, nowadays, suffer from air pollution. Two reasons for this trauma
include energy and food which will be explained in the two following paragraphs.

The main culprit for air pollution can be humans' need for energy; Cities are
polluted duo the fact that humans' need energy for various purposes such as
transportation, providing heat, etc.
Moreover, they would rather gain it by burning fossil fuels such as coal, petroleum, etc than by using environmental friendly techniques. 
Fossil oil combustion pollutes the air on account of the fact that it produces
carbon monoxide and Nitrogen oxides including NO and NO2, that are highly active and engage in other
chemical reactions resulting in smog and acid rain. 

The problem can also be seen in view of agricultural activities since they involve
a lot of insecticides, pesticides, and fertilisers usage which produce NH3, aka
Ammonia, which is the most dangerous gas in the atmosphere. 

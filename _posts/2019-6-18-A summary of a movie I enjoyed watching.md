---
title: A summary of a movie I enjoyed watching
layout: post
---
## 12 Angry Men

The fate of the 18-year-old boy was in the hands of twelve men. Allegedly, he
had killed his own father with a knife that was apparently exteremly difficult to
find and purchase after a furious argument in which the boy
had been heard telling his father "I'm gonna kill you". Now it was up to a group
of twelve men who had never met each other to decide whether the boy was guilty.

There were two witnesses to the crime. An old lady who claimed had seen the boy
commiting murder and an old man living at the next-door apartment who claimed to
have run to the door and seen the boy escaping the house with blood on his hands.

The grand jury put the case to the vote and eleven out of twelve men stated that
the boy was guilty. For the grand jury to state a final result, all of the
jurors should've accepted the result.

One man stated they must have missed something and the
details lacked precision. To begin with he drew a knife out of his pocket which
was identical to the murder weapon and challenged the rarity of the knife and
said he could buy it easily from a shop. He then asked for a second vote and one
more person voted for not guilty.

All this time they were in fight with each other and the majority insisted on
the boy's being guilty because of the two witnesses. 
The first juror then went on to challenge the practicability of witnesses claims
and introduced a new possibility each time and asked for a revote.
With each new possibility being introduced, more people would change their vote
to not guilty.

Finally, the grand jury acquitted the boy of murder and presented the facts to
the court.

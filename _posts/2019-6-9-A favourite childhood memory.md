---
layout: post
title: A favourite childhood memory
---

## How I began going to an ELI

Ever since I was a child, I wanted to learn computer programming; thus, I would
buy different programming books, DVD tutorials, and other similar things to
learn more of it.

One day I decided that I wanted to have a private programming class; hence,
I began looking for an instructor. After a few days, I finally found the person
I believed could provide me with the knowledge I desired.
He owned a shop where he would sell various software packages, and where he did internet research, etc
Then, I asked my parents to take me to his shop and ask him to teach me
programming; for I was just a kid then and couldn't take a class by myself.
    
Eventually, my father and I went to him and my father asked whether he could
teach me how to program. To my amazement, he said that I was too young and that
I needed to attend an English class. And so I did!

I think he did me a favour by telling me to learn how to fish when I was
perplexed and only wanted a fish...
Now, knowing English, I can easily watch high quality tutorials and
study official documentaion of programming languages.

Interestingly, a few years later he became a new member of staff of the English institute I am
attending after one of the employees went to another city...


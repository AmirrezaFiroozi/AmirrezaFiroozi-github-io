---
layout: post
title: How people get married in Iran
---

Nowadays, the marriage of Iranian youth happens in the following manner:

First, the young man decides that he wants to be married and lets her mother
know that. Then, his mother begins looking for a suitable candidate for marrying
his son usually based on factors such as beauty, family-class and religiosity. 

Second, the young boy's parents arrange a meeting with the girl's parents
to discuss and examine whether the two suit each other. At this point, the man
and the woman are sent to a private room to state their expectations of their
spouse and also say any condition they think their spouse should have. 

Third, if both the girl and the boy accept each other's conditions, their parents
schedule a second meeting in which they discuss the amount of Mahrieh, the money
that the bride can have after the marriage at any time she wants! This is one of
the difficult stages at which many potential marriages are destroyed.

Now, if both sides agree to the requested conditions, they will set a date
when they will book a place and usually invite members of their family and their
close friends, where they will hold a ceremony in which the spouses officially become engaged to one another.
Although they are considered husband and wife at this point, the tradition is not quite done yet!

Finally, after some time of living together and getting to know each other
better, the couple will hold a second ceremony when they invite a vaster number
of their relatives and friends and celebrate greatly their marriage and life
together.

---
layout: post
title: Differences between emails and letters
---

There are numerous marked differences between traditional letters and modern
emails including the following:

First, unlike emails, letters are physical papers one can touch and are hand-written by a pen. They are also posted physically to a real, world destination whereas emails are typed and are posted electronically to a virtual address. 

Second, letters require postage stamps attached to them so that they can be sent
and are paid whereas emails do not need stamps of any kind and are available for
free. 

Third, letters require a rather long time to be received and their status,
whether they have been received, is unknown for a period of time before the
receiver gets them. In contrast, emails are sent in a twinkling and the sender
immediately gets notified if anything goes wrong.

last but not least, letters are considered more formal than emails and in most cases must
follow a strict format while emails are less formal and the formatting is less
strict; thus, letters are paid more attention to when they are received. 

These were four of the many differences between emails and letters. 
It is important to know the purpose, time and the degree of formality when
deciding the method of sending one's writing. 

---
layout: post
title: How to get a driver's licence
---


You, as an Iranian, can obtain your driving licence by undergoing the following process:

First, you should sign up at a driving school and attend theoretical classes in which you are partially taught a book of driving laws and driving guidelines.

Then you should finish studying the remaining chapters of the book by yourself and sit the theoretical exam. Now, hopefully, if you pass, you'll need to choose a driving instructor with whom you feel
comfortable, who will teach you how to drive a car for twelve sessions.
At this point, you should practise various driving skills such as how to park your vehicle, how to take a left or right turn, etc.

Finally, you have to pass the driving test after which you'll be granted your driving licence.

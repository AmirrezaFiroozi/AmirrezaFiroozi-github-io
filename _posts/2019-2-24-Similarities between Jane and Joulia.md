---
layout: post
title: Similarities between Jane and Joulia
---

The 25-year-old Jane and her college, Joulia, are similar in several ways including their
appearance, choice of clothes, career, how they met their husbands, their taste
for food. 

Jane resembles Joulia greatly.
Jane is green-eyed, red-haired, white-skinned. As is Joulia. 
Jane is 178 cenimeters tall, similarly Joulia is 178 centimeters tall. 
Jane has a tiny scar on her forehead duo to a bike accident, Joulia has one duo
to a similar accident.

Both Jane and Joulia are practising law. 
Jane decided to defend defenseless people and become a lawyer after watching 12 Angry Men. Joulia was influenced by the same movie and became a lawyer.

Jane married a man named Taylor when she was 23 years old, whome she first met
at a flower shop. Srikingly similar is
how Joulia married her husband when she was 23 years of age. 

They also have a similar taste for food. Jane is a vegetarian and dislikes meat.
Joulia is likewise considered a vegetarian. Jane loves pizza and burrito. Joulia
loves the same foods.

As mentioned, Jane and Joulia are much alike when it comes to appearance, choice of clothes, career, and taste.

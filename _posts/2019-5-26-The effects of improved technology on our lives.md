---
layout: post
title: The effects of improved technology on our lives
---



Improved technology has affected our lives in countless ways from which I can name the following:

Advanced technology has helped us to produce antibiotics and vaccines to fight deadly diseases before they even begin to show signs.
We no longer suffer from illnesses such as the Plague, the infamous Black Death, which took the lives of nearly two hundred million people.

Moreover, improved technology has led to the development of modern vehicles and consequently incredibly faster transportation. Thanks to these vehicles, we no longer have to spend one whole month travelling, only to get to a city in a neighbouring province.

In addition, improved technology has enabled us to communicate more easily and within vast distances.
A college student emails an application form from hundreds of miles away to a teacher hoping that it will get accepted, People call their relatives living in other cities, hear their voices, etc.

Furthermore, advanced technology is responsible for not only industrial but also home automation. How many times have you asked your voice assistant to order food for you or to turn off the lamps?

Last but not least, the improvement of technology and the development of the Web has resulted in easier access to more detailed information. What took a week to find at a library can now be found within seconds on the net using a search engine.

